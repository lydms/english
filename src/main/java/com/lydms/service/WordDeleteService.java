package com.lydms.service;


import org.springframework.stereotype.Service;

@Service
public interface WordDeleteService {

    /**
     * 根据ID进行删除
     * @param englishId
     * @return
     */
    boolean deleteID(Integer englishId);

    /**
     * 根据英文进行删除
     * @param english
     * @return true :删除成功
     * @return false :删除失败
     */
    boolean deleteEnglish(String english);



}
