package com.lydms.service;


import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface WordExcelService {

    List<Map> getLocalExcel() throws Exception;

}
