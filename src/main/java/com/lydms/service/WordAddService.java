package com.lydms.service;


import com.lydms.pojo.English;
import com.lydms.pojo.Timing;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WordAddService {

    /**
     * 添加英语单词
     * @return English.english有数据：正常
     * @return English.english无数据：存入数据库异常
     * @return null：数据库中已存入当前单词
     */
    English addEnglish(English en);

    /**
     * 批量添加英语
     */
    JSONObject addBatchEnglish(English en);

    /**
     * 通过文件添加英语
     */
    JSONObject addFile(byte[] bytes);

    /**
     * 添加汉语
     * @param en
     * @return
     */
    English addChinese(English en);


    /**
     * 2.3、存入数据库
     * @param en
     * @return
     */
    int saveDataBase(English en);

    /**
     * 插入定时英语
     */
    JSONObject addTimingEnglish(String english);

    /**
     * 插入定时英语
     */
    JSONObject addTimingEnglish(List<English> enList);


    /**
     * 对day进行检测（1~30）
     * @param day
     * @return
     */
    Integer checkDay(Integer day);

    /**
     * 向数据库中插入数据
     */
    int addTiming(Timing timing);
}
