package com.lydms.service;


import org.springframework.stereotype.Service;

@Service
public interface WordSmsService {


    String sendSms(String phone, Integer code);

}
