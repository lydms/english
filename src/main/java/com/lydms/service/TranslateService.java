package com.lydms.service;


import org.springframework.stereotype.Service;

@Service
public interface TranslateService {


    /**
     * 进行翻译
     * @param english
     * @param transPort
     * @return
     */
    String translate(String english,String transPort);


}
