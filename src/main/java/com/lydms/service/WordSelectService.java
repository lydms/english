package com.lydms.service;


import com.lydms.pojo.English;
import com.lydms.pojo.PageResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WordSelectService {
    /**
     * 查询所有
     * @return
     */
    List<English> selectAll();

    /**
     * 分页查询
     * @param pageNum   起始索引
     * @param pageSize  查询的条数
     * @return
     */
    PageResult findPage(int pageNum,int pageSize);

    /**
     * 根据英语进行查询
     * @param english
     * @return
     */
    English selectOneByEnglish(String english);

    /**
     * 根据汉语进行查询
     * @param chinese
     * @return
     */
    English selectOneByChinese(String chinese);

    /**
     * 根据ID进行查询
     * @param englishId
     * @return
     */
    English selectOneById(int englishId);

    /**
     * 根据Id进行下一个（上一个）
     * @param englishId
     * @param remark    1位下一个，0位上一个；
     * @return
     */
    English selectNear(int englishId,int remark);

    /**
     * 根据分类信息进行查询
     * 0:未分类
     * 1：开发
     * 2：学习
     * @return
     */
    List<English> selectCategory(int category);

    /**
     * 查询分类2，指定天的数据（未发则发送今天的数据）
     * @param day
     * @return
     */
    List<English> selectToday(String day);

    /**
     * 根据分类查询执行日期的数据
     * 未传入日期，则查询当天数据
     * @param category
     * @param day
     * @return
     */
    List<English> selectDay(Integer category, Integer day);
}
