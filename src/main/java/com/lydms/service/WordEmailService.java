package com.lydms.service;


import com.lydms.pojo.Email;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WordEmailService {


    void sendSimpleMail(String toMail, String subject, String content);

    /**
     * 发送Html格式的邮件
     * @param toMail    邮箱地址
     * @param subject   邮件简介
     * @param content   发送的Html格式内容
     */
    void sendHtmlMail(String toMail, String subject, String content);

    void sendHtmlPhotoMail(String to, String subject, String content, List<Email> resourceist);

    void sendFileMail(String toMail, String subject, String content, String filePath);

    /**
     * 查询指定分类2天数的数据（不传返回当天数据）
     *
     * @param day
     * @return Html类型的当天数据
     */
    String getHtmlEmailByDay(String day);

}