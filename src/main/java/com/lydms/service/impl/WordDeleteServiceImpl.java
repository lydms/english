package com.lydms.service.impl;

import com.lydms.dao.EnglishDao;
import com.lydms.pojo.EnglishQuery;
import com.lydms.pojo.EnglishQuery.Criteria;
import com.lydms.service.WordDeleteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WordDeleteServiceImpl implements WordDeleteService {
    private static final Logger logger = LogManager.getLogger(WordDeleteServiceImpl.class);

    @Autowired
    private EnglishDao englishDao;


    @Override
    public boolean deleteID(Integer englishId) {
        int result = englishDao.deleteByPrimaryKey(englishId);
        logger.info("删除主键:{},返回值为===：{}",englishId,result);
        if (result==1){
            return true;
        }
        return false;
    }


    /**
     *根据英文进行删除
     * @param english
     * @return
     */
    @Override
    public boolean deleteEnglish(String english) {
        EnglishQuery query = new EnglishQuery();
        Criteria criteria = query.createCriteria();
        criteria.andEnglishEqualTo(english);
        int result = englishDao.deleteByExample(query);
        if (result==1){
            return true;
        }
        return false;
    }
}
