package com.lydms.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lydms.dao.EnglishDao;
import com.lydms.pojo.English;
import com.lydms.pojo.EnglishQuery;
import com.lydms.pojo.EnglishQuery.Criteria;
import com.lydms.pojo.PageResult;
import com.lydms.service.WordSelectService;
import com.lydms.utils.PageUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class WordSelectServiceImpl implements WordSelectService {
    private static final Logger logger = LogManager.getLogger(WordSelectServiceImpl.class);

    @Autowired
    private EnglishDao englishDao;

    @Value(value = "${emailCategory}")
    private int emailCategory;

    /**
     * 查询所有
     *
     * @return
     */
    @Override
    public List<English> selectAll() {
        List<English> en = englishDao.selectByExample(null);
        logger.info("查询到的数据为:{}", en.toString());
        return en;
    }


    /**
     * 分页查询
     *
     * @return
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {
//        1、分页从数据库中查询
        PageHelper.startPage(pageNum, pageSize);
        List<English> sysMenus = englishDao.selectPage();
        logger.info("查询数据库的返回值为=====" + sysMenus.toString());
//        2、对返回值进行判断
        if (sysMenus.size() == 0) {
            return null;
        }
//        3、对查询的结果进行封装
        PageInfo<English> englishPageInfo = new PageInfo<>(sysMenus);
        PageResult pageResult = PageUtils.getPageResult(englishPageInfo);

        return pageResult;
    }


    /**
     * 根据英文单词查询单个数据
     *
     * @param english
     * @return
     */
    @Override
    public English selectOneByEnglish(String english) {
        EnglishQuery query = new EnglishQuery();
        Criteria criteria = query.createCriteria();
        criteria.andEnglishEqualTo(english);
        List<English> en = englishDao.selectByExample(query);
        if (en.size() == 0) {
            logger.info("从数据库返回的结果为null");
            return null;
        } else {
            English result = en.get(0);
            logger.info("查询到的结果为：{}", result);
            return result;
        }
    }

    /**
     * 根据汉语查询单个数据
     *
     * @param chinese
     * @return
     */
    @Override
    public English selectOneByChinese(String chinese) {
        EnglishQuery query = new EnglishQuery();
        Criteria criteria = query.createCriteria();
        criteria.andChineseEqualTo(chinese);
        List<English> ch = englishDao.selectByExample(query);
        if (ch.size() == 0) {
            logger.info("从数据库返回的结果为null");
            return null;
        } else {
            English result = ch.get(0);
            logger.info("查询到的结果为：{}", result);
            return result;
        }
    }

    /**
     * 根据ID进行查询
     *
     * @param englishId
     * @return
     */
    @Override
    public English selectOneById(int englishId) {
        English result = englishDao.selectByPrimaryKey(englishId);
        logger.info("根据ID为：{}查询出来的结果为：{}", englishId, result);
        return result;
    }


    /**
     * 根据Id进行下一个（上一个）
     *
     * @param englishId
     * @param remark    1为下一个，0为上一个；
     * @return
     */
    @Override
    public English selectNear(int englishId, int remark) {
//        查询下一个
        if (remark == 1) {
            englishId++;
            English result = selectOneById(englishId);
            return result;
//        查询上一个
        } else {
            englishId--;
            if (englishId == 0) {
                return null;
            } else {
                English result = selectOneById(englishId);
                return result;
            }
        }
    }

    /**
     * 根据标签进行数据查询
     *
     * @param category
     * @return
     */
    @Override
    public List<English> selectCategory(int category) {
        logger.info("进行查询的分类标签为:{}", category);
        EnglishQuery query = new EnglishQuery();
        Criteria criteria = query.createCriteria();
        criteria.andCategoryEqualTo(category);
        List<English> englishList = englishDao.selectByExample(query);

        logger.info("数据库查询出来的结果为:{}", englishList.toString());
        return englishList;
    }

    /**
     * 查询分类2，指定天的数据（未发则发送今天的数据）
     *
     * @param day
     * @return
     */
    @Override
    public List<English> selectToday(String day) {
        logger.info("查询分类{}的日期为:{}", emailCategory, day);
//        获取查询的天数
        if (day == null) {
            SimpleDateFormat mm = new SimpleDateFormat("dd");
            Date date = new Date();
            day = mm.format(date);
        }
        int now = Integer.parseInt(day);
//        查询当前数据
        EnglishQuery query = new EnglishQuery();
        Criteria criteria = query.createCriteria();
        criteria.andCategoryEqualTo(emailCategory);
        criteria.andDayEqualTo(now);
        List<English> englishList = englishDao.selectByExample(query);
        logger.info("查询出来的结果为:{}", englishList.toString());
        return englishList;
    }


    /**
     * 根据分类查询执行日期的数据
     * 未传入日期，则查询当天数据
     *
     * @param category
     * @param day
     * @return
     */
    @Override
    public List<English> selectDay(Integer category, Integer day) {
//        1、封装查询的数据
        EnglishQuery query = new EnglishQuery();
        Criteria criteria = query.createCriteria();
        criteria.andCategoryEqualTo(category);
//        封装day数据
        if (day == null) {
            Calendar instance = Calendar.getInstance();
            day = instance.get(Calendar.DAY_OF_MONTH);
        }
        criteria.andDayEqualTo(day);
//        2、查询数据
        List<English> englishList = englishDao.selectByExample(query);
        return englishList;
    }
}
