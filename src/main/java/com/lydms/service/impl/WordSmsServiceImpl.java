package com.lydms.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lydms.service.WordSmsService;
import com.lydms.thirdparty.AliSms;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Service
public class WordSmsServiceImpl implements WordSmsService {
    private static final Logger logger = LogManager.getLogger(WordSmsServiceImpl.class);

    @Autowired
    private AliSms aliSms;

    @Override
    public String sendSms(String phone, Integer code) {
//        1、发送短信
        String result = aliSms.sendSms(phone, code);
        logger.info("发送短信的结果为:{}",result);

//        2、结果的处理
        ObjectMapper mapper = new ObjectMapper();
        try {
            Map map = mapper.readValue(result, Map.class);
            String mess = (String) map.get("Message");
            if ("OK".equals(mess)) {
                return result;
            }
        } catch (IOException e) {
            logger.info("调用失败");
        }
        return null;
    }

}
