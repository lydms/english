package com.lydms.service;


import org.springframework.stereotype.Service;

@Service
public interface ImageOcrService {


    String ocrCommon(byte[] b);

}
