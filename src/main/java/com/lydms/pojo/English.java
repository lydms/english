package com.lydms.pojo;

import java.io.Serializable;

public class English implements Serializable {
    /**
     * 主键Id
     */
    private Integer englishid;

    /**
     * 英文
     */
    private String english;

    /**
     * 中文
     */
    private String chinese;

    /**
     * 类别（1、2、3）
     */
    private Integer category;

    /**
     * 备注
     */
    private String remark;

    /**
     * 日期（每月天数）
     */
    private Integer day;

    /**
     * 代码中的解释
     */
    private String codechinese;

    private static final long serialVersionUID = 1L;

    public Integer getEnglishid() {
        return englishid;
    }

    public void setEnglishid(Integer englishid) {
        this.englishid = englishid;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english == null ? null : english.trim();
    }

    public String getChinese() {
        return chinese;
    }

    public void setChinese(String chinese) {
        this.chinese = chinese == null ? null : chinese.trim();
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getCodechinese() {
        return codechinese;
    }

    public void setCodechinese(String codechinese) {
        this.codechinese = codechinese == null ? null : codechinese.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", englishid=").append(englishid);
        sb.append(", english=").append(english);
        sb.append(", chinese=").append(chinese);
        sb.append(", category=").append(category);
        sb.append(", remark=").append(remark);
        sb.append(", day=").append(day);
        sb.append(", codechinese=").append(codechinese);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        English other = (English) that;
        return (this.getEnglishid() == null ? other.getEnglishid() == null : this.getEnglishid().equals(other.getEnglishid()))
            && (this.getEnglish() == null ? other.getEnglish() == null : this.getEnglish().equals(other.getEnglish()))
            && (this.getChinese() == null ? other.getChinese() == null : this.getChinese().equals(other.getChinese()))
            && (this.getCategory() == null ? other.getCategory() == null : this.getCategory().equals(other.getCategory()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
            && (this.getDay() == null ? other.getDay() == null : this.getDay().equals(other.getDay()))
            && (this.getCodechinese() == null ? other.getCodechinese() == null : this.getCodechinese().equals(other.getCodechinese()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getEnglishid() == null) ? 0 : getEnglishid().hashCode());
        result = prime * result + ((getEnglish() == null) ? 0 : getEnglish().hashCode());
        result = prime * result + ((getChinese() == null) ? 0 : getChinese().hashCode());
        result = prime * result + ((getCategory() == null) ? 0 : getCategory().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        result = prime * result + ((getDay() == null) ? 0 : getDay().hashCode());
        result = prime * result + ((getCodechinese() == null) ? 0 : getCodechinese().hashCode());
        return result;
    }
}