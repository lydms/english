package com.lydms.pojo;

public class Email {

    private String cid;
    private String path;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Email(String cid, String path) {
        this.cid = cid;
        this.path = path;
    }

    @Override
    public String toString() {
        return "Email{" +
                "cid='" + cid + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
