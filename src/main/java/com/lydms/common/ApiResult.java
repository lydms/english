package com.lydms.common;

import java.io.Serializable;

public class ApiResult<T> implements Serializable {

	private int status;	//状态码
	private String errmsg;	//状态信息
	private T data;		//返回的数据

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public static <T> ApiResult<T> succ(T data) {
		ApiResult<T> result = new ApiResult<T>();
		result.setStatus(200);
		result.setErrmsg("成功");
		result.setData(data);
		return result;
	}
	
	public static <T> ApiResult<T> fail(int status, String errmsg) {
		ApiResult<T> result = new ApiResult<>();
		result.setStatus(status);
		result.setErrmsg(errmsg);
		return result;
	}

}