package com.lydms.web;

import com.lydms.common.ApiResult;
import com.lydms.pojo.English;
import com.lydms.pojo.PageResult;
import com.lydms.service.WordSelectService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author
 */
@RestController
@RequestMapping("/select")
public class WordSelectController {
    private static final Logger logger = LogManager.getLogger(WordSelectController.class);

    @Autowired
    private WordSelectService wordSelectService;


    /**
     * 1、查询全部数据
     *
     * @return
     */
    @RequestMapping(value = "/all", method = {RequestMethod.POST})
    public ApiResult selectAll() {
        logger.info("开始进行查询所有服务。。。。");

//        1、查询所有数据
        List<English> englishes = wordSelectService.selectAll();

//        2、返回值判断
        if (englishes.size() == 0) {
            return ApiResult.fail(103, "查询错误");
        }
        return ApiResult.succ(englishes);

    }


    /**
     * 2、分页查询
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/findPage", method = {RequestMethod.POST})
    public Object findPage(@RequestBody Map<String, Object> params) {
        logger.info("pageNum: {}, pageSize: {}",
                params.get("pageNum"), params.get("pageSize"));

//        1、入参的校验
//        当前页码
        int pageNum = 0;
//        每页数量
        int pageSize = 0;

        if (params.get("pageNum") != null) {
            pageNum = Integer.parseInt(params.get("pageNum").toString());
        }
        if (params.get("pageSize") != null) {
            pageSize = Integer.parseInt(params.get("pageSize").toString());
        }
        if (StringUtils.isBlank(String.valueOf(pageNum))) {
            return ApiResult.fail(105, "请传入起始索引");
        }

        if (StringUtils.isBlank(String.valueOf(pageSize))) {
            return ApiResult.fail(105, "请传入查询的条数");
        }

//        2、查询单个数据
        PageResult findPageResult = wordSelectService.findPage(pageNum, pageSize);

//        3、返回值判断
        if (findPageResult == null) {
            return ApiResult.fail(103, "查询失败");
        }
        return ApiResult.succ(findPageResult);
    }


    /**
     * 3、根据英文查询单个数据
     *
     * @param en
     * @return
     */
    @RequestMapping(value = "/oneByEnglish", method = {RequestMethod.POST})
    public ApiResult selectOneByEnglish(@RequestBody Map<String, Object> en) {
        logger.info("englishId: {}, English: {}, chinese: {}, remark: {}",
                en.get("englishId"), en.get("english"), en.get("chinese"), en.get("remark"));

//        1、入参的校验
        String english = null;
        if (en.get("english") != null) {
            english = (String) en.get("english");
        }
        if (StringUtils.isBlank((String) en.get("english"))) {
            return ApiResult.fail(105, "请传入英语单词");
        }

//        2、查询单个数据
        English selectOneResult = wordSelectService.selectOneByEnglish(english);

//        3、返回值判断
        if (selectOneResult == null) {
            return ApiResult.fail(103, "数据库没有当前内容");
        }
        return ApiResult.succ(selectOneResult);

    }


    /**
     * 4、根据中文查询单个数据
     *
     * @param ch
     * @return
     */
    @RequestMapping(value = "/oneByChinese", method = {RequestMethod.POST})
    public ApiResult selectOneByChinese(@RequestBody Map<String, Object> ch) {
        logger.info("englishId: {}, English: {}, chinese: {}, remark: {}",
                ch.get("englishId"), ch.get("english"), ch.get("chinese"), ch.get("remark"));

//        1、入参的校验
        String chinese = null;
        if (ch.get("chinese") != null) {
            chinese = (String) ch.get("chinese");
        }
        if (StringUtils.isBlank((String) ch.get("chinese"))) {
            return ApiResult.fail(105, "请传入中文");
        }

//        2、查询单个数据
        English selectOneResult = wordSelectService.selectOneByChinese(chinese);

//        3、返回值判断
        if (selectOneResult == null) {
            return ApiResult.fail(103, "数据库没有当前内容");
        }
        return ApiResult.succ(selectOneResult);

    }


    /**
     * 5、根据ID查询单个数据
     *
     * @param en
     * @return
     */
    @RequestMapping(value = "/oneById", method = {RequestMethod.POST})
    public ApiResult selectOneById(@RequestBody Map<String, Object> en) {
        logger.info("englishId: {}, English: {}, chinese: {}, remark: {}",
                en.get("englishId"), en.get("english"), en.get("chinese"), en.get("remark"));

//        1、入参的校验
        int englishId = 0;
        if (en.get("englishId") != null) {
            englishId = Integer.parseInt((String) en.get("englishId"));
        }
        if (StringUtils.isBlank((String) en.get("englishId"))) {
            return ApiResult.fail(105, "请传入ID");
        }

//        2、查询单个数据
        English result = wordSelectService.selectOneById(englishId);

//        3、返回值判断
        if (result == null) {
            return ApiResult.fail(103, "数据库没有当前内容");
        }
        return ApiResult.succ(result);

    }


    /**
     * 6、根据英文查询临近
     *
     * @param en
     * @return
     */
    @RequestMapping(value = "/near", method = {RequestMethod.POST})
    public ApiResult selectNear(@RequestBody Map<String, Object> en) {
        logger.info("englishId: {}, English: {}, chinese: {}, remark: {}",
                en.get("englishId"), en.get("english"), en.get("chinese"), en.get("remark"));

//        1、入参的校验
        int englishId = 0;
        int remark = 0;
        if (en.get("englishId") != null) {
            englishId = Integer.parseInt((String) en.get("englishId"));
        }
        if (en.get("remark") != null) {
            remark = Integer.parseInt((String) en.get("remark"));
        }
        if (StringUtils.isBlank((String) en.get("englishId"))) {
            return ApiResult.fail(105, "请传入ID");
        }
        if (StringUtils.isBlank((String) en.get("remark"))) {
            return ApiResult.fail(105, "请传入ID");
        }
        if (remark == 0) {
            if ((englishId - 1) == 0) {
                return ApiResult.fail(105, "不能查询出索引为0的值");
            }
        }

//        2、查询单个数据
        English result = wordSelectService.selectNear(englishId, remark);

//        3、返回值判断
        if (result == null) {
            return ApiResult.fail(103, "数据库没有当前内容");
        }
        return ApiResult.succ(result);
    }


    /**
     * 7、根据分类信息查询全部
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/category", method = {RequestMethod.POST})
    public Object selectCategory(@RequestBody Map<String, Object> params) {
        logger.info("english: {}, chinese: {},category: {}, remark: {}",
                params.get("english"), params.get("chinese"), params.get("category"), params.get("remark"));

        //        1、入参的校验
        Integer category = null;
        if (params.get("category") != null) {
            category = Integer.parseInt(params.get("category").toString());
        }
        if (StringUtils.isBlank(String.valueOf(category))) {
            return ApiResult.fail(105, "请传入分类信息");
        }

//        2、查询单个数据
        List<English> englishList = wordSelectService.selectCategory(category);
//        3、返回值判断
        if (englishList == null) {
            logger.info("查询失败");
            return ApiResult.fail(103, "查询失败");
        }
        return ApiResult.succ(englishList);
    }


    /**
     * 8、根据分类信息查询全部
     *
     * @param params 入参：category和day
     *               根据传入的时间日期进行查询（未传入时间，则查询当天的数据）
     * @return
     */
    @RequestMapping(value = "/day", method = {RequestMethod.POST})
    public Object selectDay(@RequestBody Map<String, Object> params) {
        logger.info("english: {}, chinese: {},category: {}, remark: {}",
                params.get("english"), params.get("chinese"), params.get("category"), params.get("remark"));

        //        1、入参的校验
        Integer category = null;
        Integer day = null;
        if (params.get("category") != null) {
            category = Integer.parseInt(params.get("category").toString());
        }
        if (StringUtils.isBlank(String.valueOf(category))) {
            return ApiResult.fail(105, "请传入分类信息");
        }
        day = Integer.parseInt(params.get("category").toString());

//        2、查询单个数据
        List<English> englishList = wordSelectService.selectDay(category, day);
//        3、返回值判断
        if (englishList == null) {
            logger.info("查询失败");
            return ApiResult.fail(103, "查询失败");
        }
        return ApiResult.succ(englishList);
    }


    /**
     * 测试能否调用成功
     */
    @RequestMapping(value = "/test", method = {RequestMethod.POST})
    public ApiResult test() {

        return ApiResult.succ("调用成功");
    }


}


