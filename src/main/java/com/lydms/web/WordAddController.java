package com.lydms.web;

import com.lydms.common.ApiResult;
import com.lydms.pojo.English;
import com.lydms.service.WordAddService;
import net.minidev.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


/**
 * @author
 */
@RestController
@RequestMapping("/add")
public class WordAddController {
    private static final Logger logger = LogManager.getLogger(WordAddController.class);

    @Autowired
    private WordAddService wordAddService;


    /**
     * 添加英语
     *
     * @param en
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/english", method = {RequestMethod.POST})
    public ApiResult addEnglish(@RequestBody English en) {
        logger.info("english: {}, chinese: {}, category: {}, remark: {}, day: {}",
                en.getEnglishid(), en.getEnglish(), en.getChinese(), en.getRemark(), en.getDay());

//        1、入参的校验
        String eng = null;
        if (en.getEnglish() != null) {
            eng = en.getEnglish();
        }
        if (StringUtils.isBlank(en.getEnglish())) {
            return ApiResult.fail(105, "请传入英语单词");
        }
        Integer category = null;
        if (en.getCategory() != null) {
            category = en.getCategory();
        }
        if (en.getCategory() == null) {
            return ApiResult.fail(105, "请传入Category分类信息");
        }
        if (!(category >= 1 && category <= 3)) {
            return ApiResult.fail(105, "分类信息仅支持传入1~3的数值");
        }
        if (category == 2) {
            Integer day = en.getDay();
            if (day == null) {
                return ApiResult.fail(105, "分类2需要传入相应的定时日期");
            }
            if (!(day >= 1 && day <= 30)) {
                return ApiResult.fail(105, "分类2需要传入1~30天之内的数据");
            }
        }

//        2、将单词存入数据库
        English result = wordAddService.addEnglish(en);

//        3、对返回值进行判断
        if (result == null) {
            logger.info(en.getEnglish() + "在数据库中已经存在");
            return ApiResult.fail(105, "数据库中已存入当前单词");
        } else {
            if ("2".equals(result.getRemark())) {
                logger.info("数据翻译失败");
                return ApiResult.fail(103, "数据翻译失败");
            }
            if ("3".equals(result.getRemark())) {
                logger.info("存入数据库异常");
                return ApiResult.fail(103, "存入数据库异常");
            }
        }
        return ApiResult.succ(result);
    }


    /**
     * 添加汉语
     *
     * @param en
     * @return
     */
    @RequestMapping(value = "/chinese", method = {RequestMethod.POST})
    public ApiResult addChinese(@RequestBody English en) {
        logger.info("englishId: {}, english: {}, chinese: {}, remark: {}",
                en.getEnglishid(), en.getEnglish(), en.getChinese(), en.getRemark());

//        1、入参的校验
        if (StringUtils.isBlank(en.getChinese())) {
            return ApiResult.fail(105, "请传入中文");
        }

//        2、将单词存入数据库
        English result = wordAddService.addChinese(en);

//        3、对返回值进行判断
        if (result == null) {
            logger.info(en.getEnglish() + "在数据库中已经存在");
            return ApiResult.fail(105, "数据库中已存入当前单词");
        } else {
            String english = result.getEnglish();
            if (english == null) {
                logger.info("存入数据库异常");
                return ApiResult.fail(103, "存入数据库异常");
            }
            String chinese = result.getChinese();

            if (StringUtils.isBlank(chinese)) {
                return ApiResult.fail(103, "数据翻译失败");
            }
        }
        return ApiResult.succ(result);

    }


    /**
     * 批量添加英语
     *
     * @param en
     * @return
     */
    @RequestMapping(value = "/batchEnglish", method = {RequestMethod.POST})
    public ApiResult addBatchEnglish(@RequestBody English en) {
        logger.info("english: {}, chinese: {}, category: {}, remark: {}, day: {}",
                en.getEnglishid(), en.getEnglish(), en.getChinese(), en.getRemark(), en.getDay());

//        1、入参的校验
        String english = null;
        if (en.getEnglish() != null) {
            english = en.getEnglish();
        }
        if (StringUtils.isBlank(en.getEnglish())) {
            return ApiResult.fail(105, "请传入中文");
        }
        Integer category = null;
        if (en.getCategory() != null) {
            category = en.getCategory();
        }
        if (en.getCategory() == null) {
            return ApiResult.fail(105, "请传入Category分类信息");
        }
        if (!(category >= 1 && category <= 3)) {
            return ApiResult.fail(105, "分类信息仅支持传入1~3的数值");
        }
        if (category == 2) {
            Integer day = en.getDay();
            if (day == null) {
                return ApiResult.fail(105, "分类2需要传入相应的定时日期");
            }
            if (!(day >= 1 && day <= 30)) {
                return ApiResult.fail(105, "分类2需要传入1~30天之内的数据");
            }
        }

//        2、将单词存入数据库
        JSONObject result = wordAddService.addBatchEnglish(en);

//        3、对返回值进行判断
        if (result == null) {
            logger.info(en.getEnglish() + "请求失败");
            return ApiResult.fail(103, "请求失败");
        }
        logger.info("批量添加返回数据为:{}", result.toString());
        return ApiResult.succ(result);
    }

    /**
     * 通过文档添加英语
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/file", method = {RequestMethod.POST})
    public ApiResult addFile(@RequestParam MultipartFile file) throws IOException {
        logger.info("开始通过文件添加单词");
        byte[] bytes = null;      //文件的内容
        String fileName = null;   //文件名

//        1、入参的校验
        if (file == null) {
            logger.info("传入的file没有内容");
            return ApiResult.fail(105, "传入参数为null");
        }
        if (file.getBytes() != null) {
            bytes = file.getBytes();
        }
        if (file.getOriginalFilename() != null) {
            fileName = file.getOriginalFilename();
        }
        logger.info("添加的文件为:{}", fileName);
//        2、将单词存入数据库
        JSONObject result = wordAddService.addFile(bytes);

//        3、对返回值进行判断
        if (result == null) {
            logger.info(fileName + "请求失败");
            return ApiResult.fail(103, "请求失败");
        }
        logger.info("批量添加返回数据为:{}", result.toString());
        return ApiResult.succ(result);
    }


    /**
     * 添加定时查询英语
     *
     * @param en
     * @return
     */
    @RequestMapping(value = "/timingEnglish", method = {RequestMethod.POST})
    public ApiResult addTimingEnglish(@RequestBody English en) {
        logger.info("english: {}",en.getEnglish());

//        1、入参的校验
        String english = null;
        if (en.getEnglish() != null) {
            english = en.getEnglish();
        }
        if (StringUtils.isBlank(en.getEnglish())) {
            return ApiResult.fail(105, "请传入英语");
        }

//        2、将单词存入数据库
        JSONObject result = wordAddService.addTimingEnglish(english);

//        3、对返回值进行判断
        if (result == null) {
            logger.info(en.getEnglish() + "请求失败");
            return ApiResult.fail(103, "请求失败");
        }
        logger.info("批量添加返回数据为:{}", result.toString());
        return ApiResult.succ(result);
    }


    /**
     * 测试能否调用成功
     */
    @RequestMapping(value = "/test", method = {RequestMethod.POST})
    public ApiResult test() {

        return ApiResult.succ("调用成功");
    }

}