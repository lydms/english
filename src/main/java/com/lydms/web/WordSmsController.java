package com.lydms.web;

import com.lydms.common.ApiResult;
import com.lydms.service.WordSmsService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author
 */
@RestController
@RequestMapping("/sms")
public class WordSmsController {
    private static final Logger logger = LogManager.getLogger(WordSmsController.class);

    @Autowired
    private WordSmsService wordSmsService;


    /**
     * 查询单个数据
     *
     * @param sms
     * @return
     */
    @RequestMapping(value = "/sendSms", method = {RequestMethod.POST})
    public ApiResult selectOne(@RequestBody Map<String, Object> sms) {
        logger.info("phone: {}, code: {}",
                sms.get("phone"), sms.get("code"));

//        1、入参的校验
        String phone = null;
        if (sms.get("phone") != null) {
            phone = (String) sms.get("phone");
        }
        Integer code = null;
        if (sms.get("code") != null) {
            code = Integer.valueOf((String) sms.get("code"));
        }
        if (StringUtils.isBlank((String) sms.get("phone"))) {
            return ApiResult.fail(105, "请传入手机号");
        }
        if (StringUtils.isBlank((String) sms.get("code"))) {
            return ApiResult.fail(105, "请传入验证码");
        }


//        2、查询单个数据
        String result = wordSmsService.sendSms(phone, code);
//        3、返回值判断
        if (result == null) {
            return ApiResult.fail(103, "发送失败");
        }
        return ApiResult.succ(result);

    }


    /**
     * 测试能否调用成功
     */
    @RequestMapping(value = "/test", method = {RequestMethod.POST})
    public ApiResult test() {

        return ApiResult.succ("调用成功");
    }


}


