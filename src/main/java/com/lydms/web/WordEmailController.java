package com.lydms.web;

import com.lydms.common.ApiResult;
import com.lydms.pojo.English;
import com.lydms.pojo.Email;
import com.lydms.service.WordEmailService;
import com.lydms.service.WordSelectService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author
 */
@RestController
@RequestMapping("/test")
public class WordEmailController {
    private static final Logger logger = LogManager.getLogger(WordSmsController.class);

    @Autowired
    private WordEmailService mailService;

    @Autowired
    private WordSelectService wordSelectService;

    /**
     * 查询单个数据
     *
     * @param sms
     * @return
     */
    @RequestMapping(value = "/sendSms", method = {RequestMethod.POST})
    public ApiResult selectOne(@RequestBody Map<String, Object> sms) {
        logger.info("phone: {}, code: {}",
                sms.get("phone"), sms.get("code"));

//        1、入参的校验
        String phone = null;
        if (sms.get("phone") != null) {
            phone = (String) sms.get("phone");
        }
        Integer code = null;
        if (sms.get("code") != null) {
            code = Integer.valueOf((String) sms.get("code"));
        }
        if (StringUtils.isBlank((String) sms.get("phone"))) {
            return ApiResult.fail(105, "请传入手机号");
        }
        if (StringUtils.isBlank((String) sms.get("code"))) {
            return ApiResult.fail(105, "请传入验证码");
        }


//        2、查询单个数据
//        3、返回值判断
        return null;
    }


    /**
     * 测试普通邮件调用
     */
    @RequestMapping(value = "/test", method = {RequestMethod.POST})
    public ApiResult test() {

        mailService.sendSimpleMail("395092734@qq.com", "这是一个测试邮件", "这是一个测试邮件");

        return ApiResult.succ("调用成功");
    }

    /**
     * 测试Html格式调用
     */
    @RequestMapping(value = "/testHtml", method = {RequestMethod.POST})
    public ApiResult testHtml() {

        List<English> en = wordSelectService.selectToday(null);

        String start = "<!DOCTYPE html><html><head><meta charset=\"utf-8\"><title></title></head><body><font color=\"red\">定时发送</font><table border=\"1\"><tr><th>序号</th><th>英文</th><th>标准注解</th><th>代码中注解</th></tr>";

        String prime = "";
        for (int i = 0; i < en.size(); i++) {
            English english1 = en.get(i);
            String center = "<tr><td>English</td><td>Chinese</td><td>CodeChinese</td></tr>";
            String one = center.replaceAll("English", english1.getEnglish());
            String two = one.replaceAll("Chinese", english1.getChinese());
            String result = two.replaceAll("CodeChinese", english1.getCodechinese());
            prime=prime+result;

        }

        String end = "</table></body></html>";

        String html = start + prime + end;

        mailService.sendHtmlMail("395092734@qq.com", "定时邮件发送", html);

        return ApiResult.succ("调用成功");
    }


    /**
     * 测试Html带图片
     */
    @RequestMapping(value = "/testHtmlPoto", method = {RequestMethod.POST})
    public ApiResult testHtmlPoto() {
        String html = "<!DOCTYPE html>\r\n" +
                "<html>\r\n" +
                "<head>\r\n" +
                "<meta charset=\"UTF-8\">\r\n" +
                "<title>Insert title here</title>\r\n" +
                "</head>\r\n" +
                "<body>\r\n" +
                "<img src=\"cid:image1\"/> " +
                "<img src=\"cid:image2\"/> " +
                "	<font color=\"red\">发送html</font>\r\n" +
                "</body>\r\n" +
                "</html>";
        List<Email> list = new ArrayList<Email>();
        String path = WordSmsController.class.getClassLoader().getResource("image.jpg").getPath();

        Email resource = new Email("image1", path);
        Email resource2 = new Email("image2", path);

        list.add(resource2);
        list.add(resource);
        mailService.sendHtmlPhotoMail("1432613171@qq.com", "这是一个测试邮件", html, list);


        return ApiResult.succ("调用成功");
    }


    /**
     * 测试附件调用
     */
    @RequestMapping(value = "/testFile", method = {RequestMethod.POST})
    public ApiResult testFile() {

        String html = "<!DOCTYPE html>\r\n" +
                "<html>\r\n" +
                "<head>\r\n" +
                "<meta charset=\"UTF-8\">\r\n" +
                "<title>Insert title here</title>\r\n" +
                "</head>\r\n" +
                "<body>\r\n" +
                "	<font color=\"red\">发送html</font>\r\n" +
                "</body>\r\n" +
                "</html>";
        String path = WordSmsController.class.getClassLoader().getResource("image.jpg").getPath();
        mailService.sendFileMail("395092734@qq.com", "这是一个测试邮件", html, path);

        return ApiResult.succ("调用成功");
    }

    public static void main(String[] args) {
        String center = "<tr><td>English</td><td>Chinese</td><td>CodeChinese</td></tr>";

        String one="english";
        String en = one.replaceAll("en", "00");
        System.out.println(en);


        SimpleDateFormat mm = new SimpleDateFormat("dd");
        Date date = new Date();
        String format = mm.format(date);
        System.out.println(format);
    }


}


