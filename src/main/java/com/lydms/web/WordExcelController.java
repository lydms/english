package com.lydms.web;

import com.lydms.common.ApiResult;
import com.lydms.pojo.English;
import com.lydms.service.WordAddService;
import com.lydms.service.WordExcelService;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author
 */
@RestController
@RequestMapping("/excel")
public class WordExcelController {
    private static final Logger logger = LogManager.getLogger(WordExcelController.class);


    @Autowired
    private WordExcelService wordExcelService;

    @Autowired
    private WordAddService wordAddService;



    /**
     * 通过EXCEL表格进行添加
     */
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public ApiResult test() throws Exception {
//        1、获取本地的数据
        List<Map> localExcel = wordExcelService.getLocalExcel();

//        2、本地数据处理
        ArrayList<English> englishList = new ArrayList<>();
        for (int i = 0; i < localExcel.size(); i++) {
            Map map = localExcel.get(i);
            English english = new English();
            String replace = map.get("english").toString();
            english.setEnglish(replace);
            english.setChinese(map.get("chinese").toString());
            english.setCodechinese(map.get("codechinese").toString());
            englishList.add(english);
        }
//        3、插入数据库中
        JSONObject jsonResult = wordAddService.addTimingEnglish(englishList);

        return ApiResult.succ(jsonResult);
    }

}


