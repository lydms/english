package com.lydms.web;

import com.lydms.common.ApiResult;
import com.lydms.service.ImageOcrService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author
 */
@RestController
@RequestMapping("/ocr")
public class ImageOcrController {
    private static final Logger logger = LogManager.getLogger(ImageOcrController.class);

    @Autowired
    private ImageOcrService imageOcrService;


    /**
     * 查询单个数据
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/common", method = {RequestMethod.POST})
    public ApiResult ocrCommon(@RequestParam MultipartFile file) {
        String font = null;
//        1、入参的校验
        if (file == null) {
            return ApiResult.fail(105, "请传入图片");
        }

//        2、调用百度OCR
        try {
            byte[] bytes = file.getBytes();
            font = imageOcrService.ocrCommon(bytes);
        } catch (IOException e) {
           return ApiResult.fail(103,"格式转换出现错误");
        }

//        3、返回值判断
        if (font != null) {
            return ApiResult.succ(font);
        }
        return ApiResult.fail(103, "图片识别失败");
    }


    /**
     * 测试能否调用成功
     */
    @RequestMapping(value = "/test", method = {RequestMethod.POST})
    public ApiResult test() {

        return ApiResult.succ("调用成功");
    }


}


