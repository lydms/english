package com.lydms.thirdparty;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.annotation.Resources;
import java.util.Map;

//PhoneNumbers    接收短信的手机号码。
//SignName         短信签名名称
//TemplateCode    短信模板ID
//AccessKeyId     主账号AccessKey的ID
//TemplateParam   短信模板变量对应的实际值，JSON格式。
@Component
public class AliSms {
    public String sendSms(String phone, int code) {
        JSONObject json = new JSONObject();
        json.put("code", String.valueOf(code));
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAIwxTgSvJpvw1O", "feZpLSbYYHYnC1MC4U3LSRXOh3YYvY");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("SignName", "星光");
        request.putQueryParameter("TemplateCode", "SMS_165108691");

//        request.putQueryParameter("PhoneNumbers", "15911177024");
        request.putQueryParameter("PhoneNumbers", phone.toString());
//        request.putQueryParameter("TemplateParam", "{\"code\":\"1234\"}");
        request.putQueryParameter("TemplateParam", json.toString());
        try {
            CommonResponse response = client.getCommonResponse(request);
            return response.getData();
        } catch (ServerException e) {
            return null;
        } catch (ClientException e) {
            return null;
        }
    }


}