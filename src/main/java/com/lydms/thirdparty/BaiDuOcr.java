package com.lydms.thirdparty;

import com.baidu.aip.ocr.AipOcr;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * 百度通用OCR服务
 */
@Component
public class BaiDuOcr {

    //设置APPID/AK/SK
    public static final String APP_ID = "17577465";
    public static final String API_KEY = "hMylCtVnk88Kh2BWxBWDmRoN";
    public static final String SECRET_KEY = "Mmppsj6azcTu59RNoa8TpOGklhln9fOS";

    public String sample(byte[] content) {
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("language_type", "CHN_ENG");
        options.put("detect_direction", "true");
        options.put("detect_language", "true");
        options.put("probability", "true");

        //调用服务
        JSONObject jsonObject = client.accurateGeneral(content, options);
        return jsonObject.toString();
    }
}
