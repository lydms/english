package com.lydms.dao;

import com.lydms.pojo.English;
import com.lydms.pojo.EnglishQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EnglishDao {
    int countByExample(EnglishQuery example);

    int deleteByExample(EnglishQuery example);

    int deleteByPrimaryKey(Integer englishid);

    int insert(English record);

    int insertSelective(English record);

    List<English> selectByExample(EnglishQuery example);

    English selectByPrimaryKey(Integer englishid);

    int updateByExampleSelective(@Param("record") English record, @Param("example") EnglishQuery example);

    int updateByExample(@Param("record") English record, @Param("example") EnglishQuery example);

    int updateByPrimaryKeySelective(English record);

    int updateByPrimaryKey(English record);
    //    分页查询
    List<English> selectPage();
}