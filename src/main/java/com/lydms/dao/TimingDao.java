package com.lydms.dao;

import com.lydms.pojo.Timing;
import com.lydms.pojo.TimingQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TimingDao {
    int countByExample(TimingQuery example);

    int deleteByExample(TimingQuery example);

    int deleteByPrimaryKey(Integer id);

    int insert(Timing record);

    int insertSelective(Timing record);

    List<Timing> selectByExample(TimingQuery example);

    Timing selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Timing record, @Param("example") TimingQuery example);

    int updateByExample(@Param("record") Timing record, @Param("example") TimingQuery example);

    int updateByPrimaryKeySelective(Timing record);

    int updateByPrimaryKey(Timing record);
}