# english

项目JavaDoc路径：https://apidoc.gitee.com/lydms/english

### 介绍
个人英语单词查询存储服务，并且使用了MySQL，定时短信服务，fastDFS等技术进行设计；

### 个人网站地址
 [个人网站地址](http://www.lydms.com) 
 
 [接口文档地址](https://lydms.gitbook.io/englishbook/)
 
 [CSDN地址](https://blog.csdn.net/weixin_44624117)

### 实用技术
采用springboot微服务架构；

### 项目内容介绍



### 安装教程

1.  下载程序到本地；
2.  采用标准的spring boot的启动方式，进行启动项目；


### 个人留言
欢迎有爱好的个人加入进来，如果你要有好的项目，当然把我吸收进去的话最好了；
如果你能爱好学习，可以一起组个队，一起学习，相互促进，考研的当然也可以了。
这个纯属个人爱好写的。比较的简单粗浅；

### 联系方式
直接在现在评论就可以了，我会看到的。
